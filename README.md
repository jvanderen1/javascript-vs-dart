# JavaScript Vs Dart

This project demonstrates the performance differences between JavaScript and Dart. To utilize the software, you will need:

-   A `Promise` compatible [web browser](https://caniuse.com/#feat=promises)
-   An API key to [WordsAPI](https://www.wordsapi.com/)

## Experiment

Each of the examples provided will run the same experiment:
1)  Opening the `index.html` file of each program in a web browser will display the following web page (for a more detailed explanation on how to run each, please refer to `Demonstration`)

    ![Initial web page](https://imgur.com/aoo10Cl.png)

2)  Clicking the `Create List of Words` button should create a list of words with their respective definitions. Note that these words are extracted by the spaces between each word in the sentence.

    ![After definitions load](https://imgur.com/SokFPYB.png)
    
3)  With the [console log](https://zapier.com/help/how-retrieve-console-logs-your-browser-troubleshooting/) open in the browser, the program's performance can be viewed

## Demonstration

To run the provided examples, refer to the instructions laid out in each separate source directory:
-   [Dart](/dart/README.md)
-   [JavaScript](/javascript/README.md)
-   [JavaScript (with Web Workers)](/javascript-with-webworker/README.md)
