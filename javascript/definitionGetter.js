// Setup
// -----

const APIKey = "YOUR_API_KEY_HERE";

// Internal
// --------

class DefinitionGetter {
    //
    // Properties

    // Private
    _word;
    _name;

    //
    // Instance methods

    // Constructor
    constructor(word, name) {
        this._word = word;
        this._name = name;

        this.printWorkerMessage(`word received: ${ word }`);
    }

    // Methods
    getRequest = (URL, method = 'GET') => (
        new Promise((resolve) => {
            const xmlhttp = new XMLHttpRequest();

            xmlhttp.onreadystatechange = () => {
                if (xmlhttp.readyState === 4) {
                    if (xmlhttp.status === 200) {
                        const result = JSON.parse(xmlhttp.response);
                        resolve(result);
                    } else {
                        this.printWorkerMessage(`Error ${ xmlhttp.statusText }`, true);
                        const result = {
                        	word: this._word,
                            definitions: [{ 'definition': '???' }]
                        };
                        resolve(result);
                    }
                }
            };

            xmlhttp.open(method, URL);
            xmlhttp.setRequestHeader("X-RapidAPI-Host", "wordsapiv1.p.rapidapi.com");
            xmlhttp.setRequestHeader("X-RapidAPI-Key", APIKey);
            xmlhttp.send();
        })
    );

    //

    printWorkerMessage = (message, isError = false) => (
        isError ?
            console.error(`[ worker ${ this._name } ] ${ message }`) :
            console.log(`[ worker ${ this._name } ] ${ message }`)
    );

    //

    start = () => (
        new Promise((resolve) => {
            const url = `https://wordsapiv1.p.rapidapi.com/words/${ this._word }/definitions`;
            const promiseRequest = this.getRequest(url);
            promiseRequest.then((data) => (
                resolve(data)
            ));
        })
    );
}
