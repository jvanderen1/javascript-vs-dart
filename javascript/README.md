# JavaScript

This demonstrates how to set up and run the JavaScript example.

## Software Installation

No additional software is required to run this program.

## Setup

The following demonstrates how to set up the JavaScript example for use:

1)  Within [definitionGetter.js](https://gitlab.com/jvanderen1/javascript-vs-dart/blob/master/javascript/definitionGetter.js#L4), update the string in `APIKey` to be the key provided by WordsAPI.
2)  Open `index.html` in a `Promise` compatible [web browser](https://caniuse.com/#feat=promises)
