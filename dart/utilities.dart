// Dependencies
// ------------

// Libraries
import 'dart:html';
import 'dart:core';

// Classes
import 'definitionGetter.dart';

// Internal
// --------

void createListOfDefinitions(
    dynamic wordsWithDefinitions, UListElement wordsEl) {
  wordsWithDefinitions.forEach((wordWithDefinition) {
    final word = wordWithDefinition["word"];
    final definitions = wordWithDefinition["definitions"];
    final LIElement wordEl = getElement(word);
    wordEl.text = word;
    final OListElement subList = new OListElement();

    wordsEl.append(wordEl);
    wordEl.append(subList);

    definitions.forEach((definition) {
      final childNode = getElement(definition);
      subList.append(childNode);
    });
  });
}

//

Future<List> gatherDefinitions(List<String> words) {
  final List<Future<Map>> listOfFutureDefinitions = [];

  words.asMap().forEach((index, word) {
    if (word.isNotEmpty) {
      final DefinitionGetter definitionsIsolate =
          DefinitionGetter(word: word, name: index);
      printMainMessage('worker $index started...');
      listOfFutureDefinitions.add(definitionsIsolate.start());
    }
  });

  return Future.wait(listOfFutureDefinitions);
}

//

LIElement getElement(String val) {
  final LIElement li = new LIElement();
  li.text = val;
  return li;
}

//

Future<void> getListItems(Event event) async {
  Stopwatch stopwatch = new Stopwatch();
  stopwatch.start();

  final UListElement wordsEl = querySelector('#words') as UListElement;
  final InputElement inputEl = querySelector('#input') as InputElement;
  final String wordsStr = inputEl.value;
  final List<String> wordsList = wordsStr.split(' ');

  removeChildNodes(wordsEl);
  final wordsWithDefinitions = await gatherDefinitions(wordsList);
  printMainMessage('All workers complete!');
  createListOfDefinitions(wordsWithDefinitions, wordsEl);
  printMainMessage('Time elapsed: ${stopwatch.elapsedMilliseconds}ms');
  stopwatch.stop();
}

//

void printMainMessage(message) => (print('[ main.dart.js ]: $message'));

//

void removeChildNodes(UListElement el) {
  el.children.clear();
}
