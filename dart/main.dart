// Dependencies
// ------------

// Libraries
import 'dart:html';
// Utilities
import 'utilities.dart';

// Internal
// --------

void main() {
  final ButtonElement button = querySelector('#create-list') as ButtonElement;
  button.onClick.listen(getListItems);
}
