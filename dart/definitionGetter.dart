// Dependencies
// ------------

// Libraries
import 'dart:async';
import 'dart:convert';
import 'dart:html';

// Setup
// -----

const APIKey = "YOUR_API_KEY_HERE";

// Internal
// --------

class DefinitionGetter {
  //
  // Properties

  // Public
  final String word;
  final int name;

  //
  // Instance methods

  // Constructor
  DefinitionGetter({this.word, this.name}) {
    printGetterMessage('word received: $word');
  }

  // Methods
  List<String> extractDefinitions(HttpRequest request) {
    final List<String> listOfDefinitions = [];
    final decoded = jsonDecode(request.response);
    final definitions = decoded["definitions"];
    definitions.forEach((value) {
      listOfDefinitions.add(value["definition"]);
    });
    return listOfDefinitions;
  }

  //

  Future<HttpRequest> getRequest(url) =>
      (
      HttpRequest.request(url, requestHeaders: {
        "X-RapidAPI-Host": "wordsapiv1.p.rapidapi.com",
        "X-RapidAPI-Key": APIKey
      })
      );

  //

  void printGetterMessage(message) => (print('[ worker $name ] $message'));

  //

  Future<Map> start() async {
    final Completer<Map> completer = new Completer<Map>();
    final String url =
        'https://wordsapiv1.p.rapidapi.com/words/$word/definitions';
    final Future<HttpRequest> futureRequest = getRequest(url);

    futureRequest.then(extractDefinitions).then((definitions) {
      final wordWithDefinitions = new Map();
      wordWithDefinitions.addAll({"word": word, "definitions": definitions});
      completer.complete(wordWithDefinitions);
    }).catchError((error) {
      printGetterMessage('Error $error');
      final wordWithDefinitions = new Map();
      wordWithDefinitions.addAll({
        "word": word,
        "definitions": ["???"]
      });
      completer.complete(wordWithDefinitions);
    });

    return completer.future;
  }
}
