# Dart

This demonstrates how to set up and run the Dart example.

## Software Installation

The following additional software is required to run this program:

-   [Dart (including `dart2js`)](https://webdev.dartlang.org/tools/sdk#install)

## Setup

The following demonstrates how to set up the Dart example for use:

1)  Within [definitionGetter.dart](https://gitlab.com/jvanderen1/javascript-vs-dart/blob/master/dart/definitionGetter.dart#L12), update the string in `APIKey` to be the key provided by WordsAPI.
2)  To run the Dart on web, the program will need to be compiled:
    *   `Mac`: Run `make`
    *   `Other`: Run `dart2js main.dart -O4 --minify --fast-startup --trust-primitives --omit-implicit-checks -o main.dart.js`
3)  Open `index.html` in a `Promise` compatible [web browser](https://caniuse.com/#feat=promises)
