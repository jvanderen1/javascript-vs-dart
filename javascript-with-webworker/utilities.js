// Internal
// --------

const createListOfDefinitions = (wordsWithDefinitions, wordsEl) => {
    wordsWithDefinitions.forEach((wordWithDefinitions) => {
        const word = wordWithDefinitions.word;
        const definitions = wordWithDefinitions.definitions;
        const wordEl = getElement('li', word);
        const subList = getElement('ol');

        wordsEl.appendChild(wordEl);
        wordEl.appendChild(subList);

        definitions.forEach((definitionBreakdown) => {
            const definition = definitionBreakdown.definition;
            const childNode = getElement('li', definition);
            subList.appendChild(childNode);
        });


    });
};

//

const createPromiseOfDefinitions = (word, index) => (
    new Promise((resolve) => {
        const worker = getWorker();
        const message = {
            index,
            word
        };

        worker.onmessage = (event) => {
            worker.terminate();
            printMainMessage(`worker ${ index } complete!`);
            resolve(event.data);
        };

        printMainMessage(`worker ${ index } starting...`);
        worker.postMessage(message);
    })
);

//

const gatherDefinitions = (words) => {
    const arrayOfDefinitionsPromise = [];

    words.forEach((word, index) => {
        if (word) {
            const promise = createPromiseOfDefinitions(word, index);
            arrayOfDefinitionsPromise.push(promise);
        }
    });

    return Promise.all(arrayOfDefinitionsPromise);
};

//

const getElement = (type, value) => {
    const element = document.createElement(type);

    if (value) {
        const text = document.createTextNode(value);
        element.appendChild(text);
    }

    return element;
};

//

const getListItems = () => {
    console.time('Time elapsed');

    const wordsEl = document.getElementById('words');
    const inputEl = document.getElementById('input');
    const wordsStr = inputEl.value;
    const wordsArr = wordsStr.split(' ');

    removeChildNodes(wordsEl);
    gatherDefinitions(wordsArr).then((wordsWithDefinitions) => {
        createListOfDefinitions(wordsWithDefinitions, wordsEl);
        console.timeEnd('Time elapsed');
    });
};

//

// https://stackoverflow.com/questions/21408510/chrome-cant-load-web-worker/33432215#33432215
const getWorker = () => {
    const workerAsString = `(${ definitionWorker.toString() })()`;
    const blob = new Blob([workerAsString], { type: 'text/javascript' });
    const url = URL.createObjectURL(blob);
    return new Worker(url);
};

//

const printMainMessage = (message) => (
    console.log(`[ main.js ]: ${ message }`)
);

//

const removeChildNodes = (element) => {
    while (element.firstChild) {
        element.removeChild(element.firstChild);
    }
};
