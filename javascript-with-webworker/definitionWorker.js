// Internal
// --------

const definitionWorker = () => {
    //
    // Properties

    const APIKey = "YOUR_API_KEY_HERE";
    let name = "";

    //

    self.onmessage = (message) => {
        const {
            index,
            word
        } = message.data;

        const URL = `https://wordsapiv1.p.rapidapi.com/words/${ word }/definitions`;
        name = index;

        printWorkerMessage(`word received: ${ word }`);

        makeAPICall(URL).then((result) => {
            const response = {
                word,
                definitions: result["definitions"]
            };
            postMessage(response);
        });
    };

    //

    const makeAPICall = (URL, method = 'GET') => (
        new Promise((resolve) => {
            const xmlhttp = new XMLHttpRequest();

            xmlhttp.onreadystatechange = () => {
                if (xmlhttp.readyState === 4) {
                    if (xmlhttp.status === 200) {
                        const result = JSON.parse(xmlhttp.response);
                        resolve(result);
                    } else {
                        printWorkerMessage(`Error ${ xmlhttp.statusText }`, true);
                        const result = {
                            definitions: [{ 'definition': '???' }]
                        };
                        resolve(result);
                    }
                }
            };

            xmlhttp.open(method, URL);
            xmlhttp.setRequestHeader("X-RapidAPI-Host", "wordsapiv1.p.rapidapi.com");
            xmlhttp.setRequestHeader("X-RapidAPI-Key", APIKey);
            xmlhttp.send();
        })
    );

    //

    const printWorkerMessage = (message, isError = false) => (
        isError ?
            console.error(`[ worker ${ name } ] ${ message }`) :
            console.log(`[ worker ${ name } ] ${ message }`)
    );
};
