# JavaScript (with Web Workers)

This demonstrates how to set up and run the JavaScript (with Web Worker) example.

## Software Installation

The following additional software is required to run this program:

-   In addition to being `Promise` compatible, the web browser should also be `Web worker` [compatible](https://caniuse.com/#feat=webworkers).

## Setup

The following demonstrates how to set up the Dart example for use:

1)  Within [definitionWorker.js](https://gitlab.com/jvanderen1/javascript-vs-dart/blob/master/javascript-with-webworker/definitionWorker.js#L8), update the string in `APIKey` to be the key provided by WordsAPI.
2)  Open `index.html` in a `Promise` compatible [web browser](https://caniuse.com/#feat=promises)
